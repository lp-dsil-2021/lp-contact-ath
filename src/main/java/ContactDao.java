import java.util.ArrayList;
import java.util.List;

public class ContactDao implements IContactDao {

    private List<Contact> contacts = new ArrayList<>();

    public void add(Contact contact) {
        contacts.add(contact);
    }

    public boolean isContactExist(String name) {
        //Java 8 / Stream
        return contacts.stream()
                .anyMatch(x -> name.equalsIgnoreCase(x.getName()));

        // Java 5 - Old fashioned way
        // Simuler mon SELECT WHERE name=""
        /*for (Contact contact : contacts) {
            if (name.equalsIgnoreCase(contact.getName())) {
                return true;
            }
        }

        // Java < 5 - T-Rex way
        for(int i=0; i<contacts.size(); i++){
            if(name.equalsIgnoreCase(contacts.get(i).getName())){
                return true;
            }
        }
        return false;
        */
    }
}
