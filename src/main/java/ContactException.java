public class ContactException extends Exception {

    public static final String CONTACT_FORMAT = "Le nom du contact doit faire entre 3 et 40 caractères";
    public static final String CONTACT_DUPLICATION = "Le nom du contact est déjà présent";


    public ContactException() {
        super();
    }
}
