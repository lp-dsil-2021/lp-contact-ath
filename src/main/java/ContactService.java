public class ContactService implements IContactService {

    // DAO d'accès à la base de donnée
    private IContactDao contactDao = new ContactDao();

    public void creerContact(String name) throws ContactException {
        if (name == null || name.trim().length() < 3 || name.trim().length() > 10) {
            throw new ContactException();
        }
        String trimmedName = name.trim();
        if (contactDao.isContactExist(trimmedName)) {
            throw new ContactException();
        }
        Contact contact = new Contact();
        contact.setName(trimmedName);
        contactDao.add(contact);
    }
}
