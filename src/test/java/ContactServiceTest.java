import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ContactServiceTest {

    ContactService service = new ContactService();

    @Test
    public void shouldFailIfNull() {
        assertThrows(ContactException.class, () -> service.creerContact(null));
    }

    @Test
    public void shouldFailIfEmpty() {
        assertThrows(ContactException.class, () -> service.creerContact(""));
    }

    @Test
    public void shouldFailIfBlank() {
        assertThrows(ContactException.class, () -> service.creerContact("   "));
    }

    @Test
    public void shouldFailIfUnderThree() {
        assertThrows(ContactException.class, () -> service.creerContact("ab"));
    }

    @Test
    public void shouldFailIfOverTen() {
        assertThrows(ContactException.class, () -> service.creerContact("abcdefghijk"));
    }

    @Test
    public void shouldPassOnThree() throws ContactException {
        service.creerContact("abc");
    }

    @Test
    public void shouldPassOnTen() throws ContactException {
        service.creerContact("abcdefghij");
    }

    @Test
    public void shouldFailOnExist() throws ContactException {
        service.creerContact("thierry");
        assertThrows(ContactException.class, () -> service.creerContact("thierry"));

    }
}
