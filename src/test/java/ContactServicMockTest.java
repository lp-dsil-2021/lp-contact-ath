import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ContactServicMockTest {

    @Mock
    private IContactDao contactDao;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Captor
    private ArgumentCaptor<Contact> contactCaptor;

    @Test(expected = ContactException.class)
    public void shouldFailOnDuplicateEntry() throws ContactException {
        // Définition du mock
        when(contactDao.isContactExist("Thierry"))
                .thenReturn(true);
        // Test
        contactService.creerContact("Thierry");
    }

    @Test
    public void shouldPass() throws ContactException {
        // Définition du mock

        when(contactDao.isContactExist("Thierry")).thenReturn(false);
        // Test
        contactService.creerContact("        Thierry       ");

        Mockito.verify(contactDao).add(contactCaptor.capture());
        // Je souhaite voir le contenu de l'argument
        Contact contact = contactCaptor.getValue();
        Assertions.assertEquals("Thierry", contact.getName());
    }

    public void shouldPassWithDelay() throws ContactException {
        // Définition du mock
        when(contactDao.isContactExist("Thierry"))
                .thenAnswer((Answer<Boolean>) invocationOnMock -> {
                    Thread.sleep(3000);
                    return false;
                });
        // Test
        contactService.creerContact("Thierry");
    }
}
