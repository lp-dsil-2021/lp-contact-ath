import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {

    @InjectMocks
    ContactService service = new ContactService();

    @Mock
    IContactDao contactDao;

    @Test
    public void shouldFailIfExists() {
        Mockito.when(
                contactDao.isContactExist("Arnaud")
                )
                .thenReturn(true);
        assertThrows(ContactException.class, () -> service.creerContact("Arnaud"));
    }
}
