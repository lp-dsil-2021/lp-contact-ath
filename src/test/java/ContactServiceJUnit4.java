import org.junit.Test;

public class ContactServiceJUnit4 {


    IContactService service = new ContactService();

    @Test(expected = ContactException.class)
    public void shouldFailIfNull() throws ContactException {
        service.creerContact(null);
    }

    @Test(expected = ContactException.class)
    public void shouldFailIfEmpty() throws ContactException {
        service.creerContact("");
    }

    @Test(expected = ContactException.class)
    public void shouldFailIfBlank() throws ContactException {
        service.creerContact("   ");
    }

    @Test(expected = ContactException.class)
    public void shouldFailIfUnderThree() throws ContactException {
        service.creerContact("ab");
    }
}
